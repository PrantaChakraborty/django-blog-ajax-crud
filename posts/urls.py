from django.urls import path
from .views import post_list_and_create, post_list, like_unlike_post_view, post_detail_view, post_detail_data_view, delete_post, update_post

app_name = 'posts'
urlpatterns = [
    path('', post_list_and_create, name='post-list'),
    path('posts/<int:num_of_post>/', post_list, name='post-list2'),
    path('like_unlike/', like_unlike_post_view, name='like-unlike'),
    path('<int:pk>/', post_detail_view, name='post-detail'),
    path('<int:pk>/post-detail/', post_detail_data_view, name='post-detail-data'),
    path('<int:pk>/update/', update_post, name='update-post'),
    path('<int:pk>/delete/', delete_post, name='delete-post'),
]