from django.contrib.auth.models import User
from django.db import models

from profiles.models import Profile


class Post(models.Model):
    title = models.CharField(max_length=255)
    body = models.TextField()
    like = models.ManyToManyField(User, blank=True)
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    @property
    def like_count(self):
        """ to return total like """
        return self.like.count()

    class Meta:
        ordering = ('-created',)
