from django.shortcuts import render
from django.views.generic import ListView
from .models import Post
from django.http import JsonResponse

from .forms import PostCreateForm

from profiles.models import Profile


def post_list_and_create(request):
    form = PostCreateForm(request.POST or None)
    if request.is_ajax():
        if form.is_valid():
            author = Profile.objects.get(user=request.user)
            instance = form.save(commit=False)
            instance.author = author
            instance.save()
            return JsonResponse({
                'title': instance.title,
                'body': instance.body,
                'author': instance.author.user.username,
                'id': instance.id,
            })

    context = {
        'form': form,
    }

    return render(request, 'post_list.html', context)


def post_list(request, **kwargs):
    if request.is_ajax():
        post_number = kwargs.get('num_of_post')
        visible = 1  # initially display 3 post
        upper = post_number
        lower = upper - visible
        size = Post.objects.all().count()

        qs = Post.objects.all()
        data = []
        for obj in qs:
            item = {
                'id': obj.pk,
                'author': obj.author.user.username,
                'title': obj.title,
                'body': obj.body,
                'like': True if request.user in obj.like.all() else False,
                'count': obj.like_count,
            }
            data.append(item)
        return JsonResponse({'data': data[lower:upper], "size": size})


def like_unlike_post_view(request):
    if request.is_ajax():
        pk = request.POST.get('pk')
        obj = Post.objects.get(pk=pk)
        if request.user in obj.like.all():
            liked = False
            obj.like.remove(request.user)
        else:
            liked = True
            obj.like.add(request.user)
        return JsonResponse({'liked': liked, 'count': obj.like_count})


def post_detail_view(request, pk):
    obj = Post.objects.get(pk=pk)
    form = PostCreateForm()

    context = {
        'obj': obj,
        'form': form,
    }
    return render(request, 'detail.html', context)


def post_detail_data_view(request, pk):
    obj = Post.objects.get(pk=pk)
    data = {
        'id': obj.id,
        'title': obj.title,
        'body': obj.body,
        'author': obj.author.user.username,
        'logged_in': request.user.username,
    }

    return JsonResponse({'data': data})


def update_post(request, pk):
    obj = Post.objects.get(pk=pk)
    if request.is_ajax():
        new_title = request.POST.get('title')
        new_body = request.POST.get('body')
        obj.title = new_title
        obj.body = new_body
        obj.save()

        return JsonResponse(
            {
                'title': new_title,
                'body': new_body
            }
        )


def delete_post(request, pk):
    obj = Post.objects.get(pk=pk)
    if request.is_ajax():
        obj.delete()
    return JsonResponse({})
