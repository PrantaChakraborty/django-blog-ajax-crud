const post_box = document.getElementById('post-box')
const spinner_box = document.getElementById('spinner-box')
const load_btn = document.getElementById('load-btn')
const end_box = document.getElementById('end-box')
const post_create_form = document.getElementById('post-form') // post create form
const post_title_id = document.getElementById('id_title') // post title
const post_body_id = document.getElementById('id_body') // post body
const alert_box = document.getElementById('alert-box') // post body
const csrf = document.getElementsByName('csrfmiddlewaretoken')
const url = window.location

// for csrf protect
const getCookie = (name) => {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
const csrftoken = getCookie('csrftoken');

const deleted = localStorage.getItem('title')
const handleAlerts = (type, msg) =>{
    alert_box.innerHTML =  `
        <div class="alert alert-${type}" role="alert">
            ${msg}
        </div>
    `

}
if (deleted){
    handleAlerts('danger', `deleted "${deleted}"`)
    localStorage.clear()
}


// function for trigger like unlike
const likeUnlikePosts = ()=> {
    const likeUnlikeForms = [...document.getElementsByClassName('like-unlike-forms')]  // getting the forms using id
    likeUnlikeForms.forEach(form=> form.addEventListener('submit', e=>{
        e.preventDefault()
        const clickedId = e.target.getAttribute('data-form-id')
        const clickedBtn = document.getElementById(`like-unlike-${clickedId}`)

        $.ajax({
            type: 'POST',
            url: "/like_unlike/",
            data: {
                'csrfmiddlewaretoken': csrftoken,
                'pk': clickedId,
            },
            success: function(response){
                clickedBtn.textContent = response.liked ? `Unlike (${response.count})`: `Like (${response.count})`
            },
            error: function(error){
            }
        })

    }))
}

let visible = 1
// this function is used to show post when click the load more button
const getData = () => {
    $.ajax(
        {
            type: 'GET',
            url: `/posts/${visible}`,
            success: function (response) {
                const data = response.data;
                setTimeout(() => {
                    spinner_box.classList.add('not-visible')
                    data.forEach(el => {
                        post_box.innerHTML += `
                   <div class="card mt-3" style="width: 18rem;">
                      <div class="card-body">
                        <h5 class="card-title">${el.title}</h5>
                        <p>${el.body}</p>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="col-4">
                                <a href="${url}${el.id}/" class="btn btn-primary">Details</a>
                            </div>   
                            <div class="col-6">
                              <form class="like-unlike-forms" data-form-id="${el.id}">
                                  <button class="btn btn-primary" id="like-unlike-${el.id}">${el.like ? `Unlike (${el.count})` : `Like (${el.count})`}</button>
                              </form>
                            </div>              
                        </div>
                     </div>
                     </div>
                   `
                    });
                    likeUnlikePosts()
                }, 100)
                if (response.size == 0) {
                    end_box.textContent = 'No post added yet'
                } else if (response.size <= visible) {
                    load_btn.classList.add('not-visible')
                    end_box.textContent = 'No more post to load'
                }

            },
            error: function (response) {
                console.log(response)
            }
        }
    )
}

load_btn.addEventListener('click', () => {
    spinner_box.classList.remove('not-visible')
    visible += 1
    getData()
})

post_create_form.addEventListener('submit', e=>{
    e.preventDefault()
    $.ajax(
        {
        type:'POST',
        url: '',
        data: {
            'csrfmiddlewaretoken': csrf[0].value,
            'title': post_title_id.value,
            'body': post_body_id.value,
        },
        success: function (response) {
            console.log(response)
            post_box.insertAdjacentHTML('afterbegin',
                `
                    <div class="card mt-3" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">${response.title}</h5>
                                <p>${response.body}</p>
                            </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col-4">
                                    <a href="#" class="btn btn-primary">Details</a>
                                </div>   
                                <div class="col-6">
                                  <form class="like-unlike-forms" data-form-id="${response.id}">
                                      <button class="btn btn-primary" id="like-unlike-${response.id}">Like(0)</button>
                                  </form>
                                </div>              
                            </div>
                        </div>
                    </div>
                `
            )
            likeUnlikePosts()
        //    to hide the modal
            $('#addPostModal').modal('hide')
            handleAlerts('success', 'New Post Added')
            post_create_form.reset()

        },
        error: function (error){
            $('#addPostModal').modal('hide')
            handleAlerts('danger', 'Opps, something went wrong!')
        }
    }
    )
}
)



// running it initially to show first 3 post
getData()

