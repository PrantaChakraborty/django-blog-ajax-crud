const back_btn = document.getElementById('back-btn')
const post_box = document.getElementById('post-box')
const alert_box = document.getElementById('alert-box')
const update_btn = document.getElementById('update-btn')
const delete_btn = document.getElementById('delete-btn')
const url = window.location.href + "post-detail/"
const spinner_box = document.getElementById('spinner-box')
const titleInput = document.getElementById('id_title')
const bodyInput = document.getElementById('id_body')
const updateUrl = window.location.href +"update/"
const deleteUrl = window.location.href +"delete/"
const updateFrom = document.getElementById('update-form')
const deleteFrom = document.getElementById('delete-form')
const csrf = document.getElementsByName('csrfmiddlewaretoken')

back_btn.addEventListener("click", ()=>{
    history.back()
})

$.ajax({
    type:'GET',
    url: url,
    success: function (response){
        console.log(response)
        const data = response.data
        if (data.logged_in !== data.author){
            console.log('diff')
        }
        else{
            console.log('same')
            update_btn.classList.remove('not-visible')
            delete_btn.classList.remove('not-visible')
        }
        const titleEl = document.createElement('h1') // create heading one element
        titleEl.setAttribute('class', 'mt-1') // set a class to heading one
        titleEl.setAttribute('id', 'title') // set a class to heading one
        const bodyEl = document.createElement('p') // create paragraph element
        bodyEl.setAttribute('class', 'mt-1')
        bodyEl.setAttribute('id', 'body')
        titleEl.textContent = data.title // set text to the heading
        bodyEl.textContent = data.body // set text to the paragraph
        post_box.appendChild(titleEl) // append in post box
        post_box.appendChild(bodyEl)
        titleInput.value = data.title
        bodyInput.value = data.body
        spinner_box.classList.add('not-visible') // hide the spinner
    },
    error: function (error){
        console.log(error)
    }
})

updateFrom.addEventListener('submit', e=>{
    e.preventDefault()
    const title = document.getElementById('title')
    const body = document.getElementById('body')

    $.ajax({
        type: 'POST',
        url: updateUrl,
        data: {
            csrfmiddlewaretoken: csrf[0].value,
            'title' : titleInput.value,
            'body': bodyInput.value,
        },
        success: function (response){
            console.log(response)
            handleAlerts('success', 'post has been updated')
            title.textContent = response.title
            body.textContent = response.body
        },
        error: function (error){
            handleAlerts('error', 'sorry error ocured')
            console.log(error)
        }
    })

})

deleteFrom.addEventListener('submit', e=>{
    e.preventDefault()
    $.ajax({
        type:'POST',
        url: deleteUrl,
        data: {
            csrfmiddlewaretoken: csrf[0].value
        },
        success: function (response){
            console.log(response)
            window.location.href = window.location.origin
            localStorage.setItem('title',titleInput.value)

        },
        error: function (error){
            console.log(error)
        }
    })
})



const handleAlerts = (type, msg) =>{
    alert_box.innerHTML =  `
        <div class="alert alert-${type}" role="alert">
            ${msg}
        </div>
    `

}
